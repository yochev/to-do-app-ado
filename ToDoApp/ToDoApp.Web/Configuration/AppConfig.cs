﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ToDoApp.Web.Configuration
{
    public static class AppConfig
    {
        public static string ConnectionString { get; set; }

        static AppConfig()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["ToDoAppDb"].ToString();
        }
    }
}