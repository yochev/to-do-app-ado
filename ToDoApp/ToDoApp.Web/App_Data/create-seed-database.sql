CREATE DATABASE ToDoApp3a

GO 
USE ToDoApp3a

CREATE TABLE Notes
(
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Title NVARCHAR(MAX) NOT NULL,
	[Description] NVARCHAR(MAX),
	IsDone BIT,
	DueDate DATETIME
)

GO
INSERT INTO Notes (Title, [Description], IsDone, DueDate)
VALUES ('Do My HW', 'Write repositories', 0, '2017-10-11')