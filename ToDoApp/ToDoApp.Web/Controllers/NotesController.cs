﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoApp.Data.Entities;
using ToDoApp.Data.Repositories;
using ToDoApp.Web.Configuration;

namespace ToDoApp.Web.Controllers
{
    public class NotesController : Controller
    {
        //
        // GET: /Notes/
        public ActionResult Index()
        {
            NoteRepository repository = new NoteRepository(AppConfig.ConnectionString);

            List<Note> model = repository.GetAll();
            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Note note)
        {
            if (!ModelState.IsValid)
            {
                return View(note);
            }

            NoteRepository repository = new NoteRepository(AppConfig.ConnectionString);
            repository.Insert(note);

            return RedirectToAction("Index");
        }
	}
}