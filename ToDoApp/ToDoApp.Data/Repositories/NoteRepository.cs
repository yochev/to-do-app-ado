﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApp.Data.Entities;
using System.Data;
using System.Data.SqlClient;

namespace ToDoApp.Data.Repositories
{
    public class NoteRepository
    {
        private string connectionString;

        public NoteRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Note> GetAll()
        {
            List<Note> result = new List<Note>();
            IDbConnection connection = new SqlConnection(connectionString);

            using (connection)
            {
                connection.Open();

                IDbCommand command = connection.CreateCommand();
                command.Connection = connection;
                command.CommandText =
@"SELECT * FROM Notes";

                IDataReader reader = command.ExecuteReader();
                using (reader)
                {
                    while (reader.Read())
                    {
                        Note note = new Note();
                        note.Id = (int)reader["Id"];
                        note.Title = (string)reader["Title"];
                        note.Description = (string)reader["Description"];
                        note.IsDone = (bool)reader["IsDone"];
                        note.DueDate = (DateTime)reader["DueDate"];

                        result.Add(note);
                    }
                }
            }

            return result;
        }

        public void Insert(Note note)
        {
            IDbConnection connection = new SqlConnection(connectionString);
            using (connection)
            {
                connection.Open();

                IDbCommand command = connection.CreateCommand();
                command.CommandText =
@"INSERT INTO Notes (Title, Description, IsDone, DueDate)
    VALUES (@Title, @Description, @IsDone, @DueDate)
";
                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@Title";
                parameter.Value = note.Title;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@Description";
                parameter.Value = note.Description;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@IsDone";
                parameter.Value = note.IsDone;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@DueDate";
                parameter.Value = note.DueDate;
                command.Parameters.Add(parameter);

                command.ExecuteNonQuery();
            }
        }
    }
}
